<!DOCTYPE html>
<html>
<head>
	<title>Bravura</title>
	<link rel="stylesheet" href="/static/semantic.css" />
	<link rel="stylesheet" href="/static/default.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="/static/semantic.js"></script>
	<script type="text/javascript" src="/static/default.js"></script>
% currentService = get("currentService")
% player = get("player")
    <script type="text/javascript">
		baseColor = '{{'' if currentService == None else player.services[currentService]['color']}}';
		playing = {{'true' if player.playing else 'false'}};
	</script>
</head>
<body>
	<div class="ui menu inverted black">
		<h1 class="ui header inverted">
    			<i class="headphone icon"></i>
    			<a href="/" class="content">Bravura</a>
		</h1>
% for service in player.services:
		<a class="item right" href="/{{service}}">{{service}}</a>
% end
	</div>
	<div class="second menu" style="display: none">
		<div class="ui menu playing inverted black">
			<div class="item">Now Playing</div>
			<div class="item channel clear"></div>
			<div class="item service clear"></div>
		</div>

		<div class="ui menu stop inverted black">
			<a class="item naggy" href="/stop"><i class="stop icon"></i></a>
		</div>

		<div class="ui menu volume inverted black">
			<a class="item" href="/volume/-1"><i class="volume down icon"></i></a>
			<div class="item value">Volume</div>
			<a class="item" href="/volume/1"><i class="volume up icon"></i></a>
		</div>
	</div>

% service = currentService
% if service != None:
    <div class="ui segment site black inverted">
        <h2 class="ui header centered {{service.split(".")[0]}}"><img src="/static/images/{{service}}.jpg" alt="{{service}}"></h2>
        <div class="channels">
%   for channel in player.services[service]['channels']:
            <a class="ui button channel naggy {{player.services[service]['color']}} inverted" href="/{{service}}/{{channel}}">
%       if 'image' in player.services[service]['channels'][channel]:
                <img src="{{player.services[service]['channels'][channel]['image']}}?height=194&width=194">
%       end
                {{player.services[service]['channels'][channel]["name"]}}
            </a>
%   end
        </div>
    </div>
% end
</body>
</html>
