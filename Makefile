SHELL:=/bin/bash

all: install

install: /etc/bravura.key
	@\
	if [ ! -f /etc/bravura.key ]; then \
		if [ ! -w /etc/bravura.key ]; then \
			echo "Permission denied. Please consider using sudo." ;\
			exit ;\
		fi ;\
		echo "Please provide a valid AudioAddict listen key:" ;\
		read -s key ;\
		VALID=`curl -s -I http://listen.di.fm/premium/favorites\?$$key` ;\
		if [[ "$$VALID" != *'200 OK'* ]]; then \
			echo "Invalid listen key" ;\
			exit ;\
		fi ;\
		sudo echo -n "$$key" > /etc/bravura.key ;\
	fi ;\
	BRAPATH=`pwd` ;\
	cd /tmp ;\
	sudo rm -rf mplayer.py ;\
	git clone https://github.com/baudm/mplayer.py.git ;\
	cd mplayer.py ;\
	sudo python setup.py install ;\
	cd /usr/local/bin ;\
	ln -sf $$BRAPATH/bravura.py bravura ;\
	if [ -f /etc/debian_version ]; then \
		cd /etc/init.d ;\
		ln -sf $$BRAPATH/service/debian.sh bravura ;\
		update-rc.d bravura defaults ;\
		service bravura start ;\
	else \
		if [ `uname` == "Darwin" ]; then \
			cd ~/Library/LaunchAgents ;\
			ln -sf $$BRAPATH/service/macosx.plist bravura.plist ;\
			launchctl unload bravura.plist ;\
			launchctl load bravura.plist ;\
		else \
			echo "Unable to automatically set bravura as a service." ;\
			echo "Please configure it manually." ;\
		fi ;\
	fi ;\
	cd $$PWD ;
