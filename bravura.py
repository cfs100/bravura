#!/usr/bin/python

from player import Player
from bottle import route, run, get, static_file, template, view
import os.path
import json

basePath = os.path.dirname(os.path.realpath(__file__))
player = Player()

@get('/static/<filename:re:.*>')
def static(filename):
	return static_file(filename, root='%s/static' % basePath)

@route('/volume/<value>')
def volume(value):
	volume = player.volume(float(value))
	if not volume:
		volume = 0.00
	return "%f" % volume

@route('/<service>/<channel>')
def play(service, channel):
	return player.play(service, channel), '\n'

@route('/stop')
def stop():
	return player.stop(), '\n'

@route('/look')
def look():
	return json.dumps(player.playbackInfo()), '\n'

@route('/<service>')
@view('%s/views/default.tpl' % basePath)
def list(service):
	return dict(player=player, currentService=service)

@route('/')
@view('%s/views/default.tpl' % basePath)
def default():
	return dict(player=player)

run(host='0.0.0.0', port=8066)
