#!/bin/sh
### BEGIN INIT INFO
# Provides:          bravura
# Required-Start:    $all
# Required-Stop:     $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: starts Bravura Music Service
# Description:       starts Bravura using start-stop-daemon
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
NAME=bravura
DESC="Bravura Music Service"
DAEMON=/usr/local/bin/$NAME
DAEMON_ARGS=""
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME

. /lib/lsb/init-functions

# Gracefully exit if the package has been removed.
test -x $DAEMON || exit 0

# Read configuration variable file if it is present
test -f /etc/default/$NAME && . /etc/default/$NAME

do_start() {
	start-stop-daemon --start --quiet --pidfile $PIDFILE --make-pidfile \
		--background --exec $DAEMON -- $DAEMON_ARGS
	if [ "$?" = 0 ]; then
		sleep 1
		pidofproc -p $PIDFILE $DAEMON  > /dev/null
	fi
}

do_stop() {
	start-stop-daemon --stop --quiet --pidfile $PIDFILE
}

do_status() {
	status_of_proc -p $PIDFILE "$DAEMON" $NAME && exit 0 || exit $?
}

case "$1" in
	start)
		log_daemon_msg "Starting $DESC" "$NAME"
		do_start
		log_end_msg $?
		;;

	stop)
		log_daemon_msg "Stopping $DESC" "$NAME"
		do_stop
		log_end_msg $?
		;;

	restart)
		log_daemon_msg "Restarting $DESC" "$NAME"
		do_stop
		do_start
		log_end_msg $?
		;;

	status)
		do_status
		;;
	*)
		echo "Usage: $SCRIPTNAME {start|stop|restart|status}" >&2
		exit 1
		;;
esac

exit 0
