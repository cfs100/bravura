jQuery(function () {
	function showNag(message) {
		$('.nag').fadeOut(400, function () { $(this).remove(); });
		var nag = $('<div class="ui nag fixed"><span class="title"></span><i class="close icon"></i></div>');
		if ( baseColor != '' )
			nag.addClass(baseColor);
		nag.find('.title').html(message);
		$('body').append(nag);
		nag.nag('show');
		setTimeout(function () { nag.fadeOut(400, function () { $(this).remove(); }); }, 2000);
	}
	$('a.naggy').click(function (e) {
		e.preventDefault();

		$.ajax({
			url: $(this).attr('href'),
			success: function (response) {
				showNag(response);
				showPlaybackInfo();
			}
		});

		return false;
	});

	var volumeTimer = false;
	$('.second.menu .volume a.item').click(function (e) {
		e.preventDefault();

		$.ajax({
			url: $(this).attr('href'),
			success: function (response) {
				var value = $('.second.menu .volume .value');
				value.html(parseInt(response));

				if (volumeTimer) clearTimeout(volumeTimer);
				volumeTimer = setTimeout(function () { value.html('Volume'); }, 3000);
			}
		});

		return false;
	});

	function showPlaybackInfo() {
		$.ajax({
			url: '/look',
			dataType: 'json',
			async: false,
			success: function (response) {
				$('.second.menu .clear').html('');

				for (var i in response) {
					var tag = $('.second.menu .' + i);
					if (tag.length > 0) {
						tag.html(response[i]);
					}
				}

				menu = $('.second.menu');
				menu.stop().slideDown();
			},
			error: function () {
				showNag("Unable to retrieve playback information.")
			}
		});
	}

	if (playing) {
		showPlaybackInfo();
	}
});
