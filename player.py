import subprocess
import re
import thread
import urllib2
import json
import mplayer

class Player:
	__player = None
	__listenKey = None
	__keyFile = '/etc/bravura.key'

	playing = {}
	services = {
		'rockradio.com': {'color': 'yellow', 'channels': {}},
		'jazzradio.com': {'color': 'red', 'channels': {}},
		'radiotunes.com': {'color': 'blue', 'channels': {}},
		'di.fm': {'color': 'purple', 'channels': {}}
	}

	def __init__(self):
		try:
			f = open(self.__keyFile, 'r')
			self.__listenKey = f.read().strip()
			f.close()
		except:
			print 'Put a valid ListenKey in %s' % self.__keyFile
			exit(1)

		for service in self.services:
			r = urllib2.urlopen("http://listen.%s/premium/" % service).read()
			if r:
				channels = json.loads(r)
				for channel in channels:
					self.services[service]['channels'][channel['key']] = channel

			url = "http://api.audioaddict.com/v1/%s/mobile/batch_update?stream_set_key=/channel_filters/0/channels/" % service.split(".")[0]
			req = urllib2.Request(url, "", {"Authorization": "Basic ZXBoZW1lcm9uOmRheWVpcGgwbmVAcHA="})
			r = urllib2.urlopen(req).read()
			if r:
				data = json.loads(r)
				for aux in data['channel_filters']:
					for channel in aux['channels']:
						if channel['key'] in self.services[service]['channels']:
							image = channel['images']['default'].split('{')[0]
							self.services[service]['channels'][channel['key']]['image'] = image

	def __del__(self):
		self.stop()

	def getPlaylist(self, service, channel):
		return '%s?%s' % (self.services[service]['channels'][channel]['playlist'], self.__listenKey)

	def playbackInfo(self):
		info = {}

		if "service" in self.playing:
			info["service"] = self.playing["service"]
			info["channel"] = self.services[self.playing["service"]]['channels'][self.playing["channel"]]["name"]

		return info

	def volume(self, value):
		if not self.__player:
			return False

		volume = int(self.__player.volume) + value
		if volume < 0:
			volume = 0

		if volume > 100:
			volume = 100

		self.__player.volume = volume
		return volume

	def parseStdout(self, line):
		print line
		info = line.split(':', 1)
		if info[0] != 'ICY Info':
			return

		song = dict(re.findall("(\w+)='([^']*)'",
			info[1].strip())).get('StreamTitle', '').strip()
		if song == '':
			return

		song = map(str.strip, song.split(' - ', 1))
		if len(song) != 2:
			return

		artist, song = song
		self.playing['artist'] = artist
		self.playing['song'] = song

	def stream(self, playlistUrl):
		if self.__player:
			return

		self.__player = mplayer.Player("-playlist %s" % playlistUrl, autospawn=False)
		self.__player.stdout.connect(self.parseStdout)
		self.__player.spawn()

	def play(self, service, channel):
		if service not in self.services:
			return 'Unknown service: <b>%s</b>' % service

		if channel not in self.services[service]['channels']:
			return 'Unknown channel in <b>%s</b>: <b>%s</b>' % (service, channel)

		channelName = self.services[service]['channels'][channel]['name']

		if 'channel' in self.playing and self.playing['channel'] == channel:
			return 'Already playing <b>%s</b>' % channelName

		if self.__player:
			self.stop()

		self.playing['service'] = service
		self.playing['channel'] = channel
		thread.start_new_thread(self.stream, (self.getPlaylist(service, channel),))

		return 'Starting <b>%s</b> from <b>%s</b>' % (channelName, service)

	def stop(self):
		if self.__player:
			self.__player.quit()
			self.__player = None

		if self.playing:
			self.playing = {}

		return 'Playback Stopped'
